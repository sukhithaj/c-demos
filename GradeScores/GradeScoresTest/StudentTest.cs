﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeScoresTest
{
    [TestClass]
    public class StudentTest
    {
        GradeScores.Student student = new GradeScores.Student();
        
        [TestMethod]
        public void TestCompareTo()
        {
            student.FirstName = "TERESSA";
            student.LastName = "BUNDY";
            student.Score = 88;

            GradeScores.Student newStudent = new GradeScores.Student();

            newStudent.FirstName = "MADISON";
            newStudent.LastName = "KING";
            newStudent.Score = 90;

            int result = 0;

            result = student.CompareTo(newStudent);
            Assert.IsTrue(result > 0);

            newStudent.Score = student.Score;
            result = student.CompareTo(newStudent);
            Assert.IsTrue(result < 0);

            newStudent.LastName = student.LastName;
            result = student.CompareTo(newStudent);
            Assert.IsTrue(result > 0);
        }
        
        [TestMethod]
        public void TestToString()
        {
            student.FirstName = "TERESSA";
            student.LastName = "BUNDY";
            student.Score = 88;

            Assert.AreEqual("BUNDY, TERESSA, 88", student.ToString());
        }

    }
}
