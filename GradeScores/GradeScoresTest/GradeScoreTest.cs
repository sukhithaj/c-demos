﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradeScores;
using System.IO;

namespace GradeScoresTest
{
    [TestClass]
    public class GradeScoresTest
    {
        GradeScores.GradeScores gradeScores = new GradeScores.GradeScores();

        /// <summary>
        /// Test method to parse a given line from the input text file and generate Student instance
        /// </summary>
        [TestMethod]
        public void TestParseStudent()
        {
            string studentScoreLine = "BUNDY, TERESSA, 88";

            Student student = gradeScores.parseStudent(studentScoreLine, 100);

            Assert.AreEqual(student.FirstName, "TERESSA");
            Assert.AreEqual(student.LastName, "BUNDY");
            Assert.AreEqual(student.Score, 88);

        }

        /// <summary>
        /// Test exception of gradeScores method when incorrect or non existent file name is given
        /// </summary>
        [TestMethod]
        public void TestGradeScores()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetError(sw);
                
                gradeScores.gradeScores("Non existient file");

                string errorMessage = string.Format("File not found, Please give full path of an existing file{0}", Environment.NewLine);

                Assert.AreEqual<String>(errorMessage, sw.ToString());
            }

        }

    }
}
