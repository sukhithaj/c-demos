﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeScores
{
    class Program
    {
        static void Main(string[] args)
        {
            GradeScores gradeScoresRunner = new GradeScores();

            if (!string.IsNullOrEmpty(args[0]))
            {
                string fileName = args[0];
                gradeScoresRunner.gradeScores(fileName);
            }
            else
            {
                System.Console.WriteLine("Please give the full path of file as first argument");
            }

        }
    }
}
