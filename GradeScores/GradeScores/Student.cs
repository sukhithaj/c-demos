﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeScores
{
    public class Student : IComparable<Student>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Score { get; set; }

        /// <summary>
        /// Compare student instance with another student for sorting students. 
        /// Sort first in Descending order of Score,
        /// Then by Ascending order of LastName,
        /// Then by Ascending order of FirstName
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Student other)
        {
            if (this.Score == other.Score)
            {
                if (this.LastName == other.LastName)
                {
                    return this.FirstName.CompareTo(other.FirstName);
                }
                return this.LastName.CompareTo(other.LastName);
            }
            return other.Score.CompareTo(this.Score);
        }

        /// <summary>
        /// overrided ToString() method to print the student as expected in output file
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return LastName + ", " + FirstName + ", " + Score;
        }
    }
}
