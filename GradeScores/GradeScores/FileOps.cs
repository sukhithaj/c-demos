﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeScores
{
    public class FileOps
    {
        public static string[] readFileLines(string fileName) {
            return System.IO.File.ReadAllLines(fileName);
        }

        public static void writeLinesToFile(string fileName, string[] lines) {
            System.IO.File.WriteAllLines(fileName, lines);
        }

        public static string getInputFileName(string filePath)
        {
            return Path.GetFileNameWithoutExtension(filePath);
        }

    }
}
