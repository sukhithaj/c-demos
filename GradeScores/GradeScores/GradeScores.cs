﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeScores
{
    public class GradeScores
    {   
        /// <summary>
        /// Main method to generate sorted scores file from the given input fileName
        /// </summary>
        /// <param name="fileName"></param>
        public void gradeScores(string fileName) {
            

            try
            {
                string[] lines = FileOps.readFileLines(fileName);

                List<Student> students = parseStudents(lines); 

                if (students.Count == 0)
                {
                    Console.WriteLine("Finished: Error in Input file!");
                    return;
                }

                // Sorting criteria implemented in Student.CompareTo method
                students.Sort();

                List<string> outputLines = new List<string>();
                students.ForEach(s => outputLines.Add(s.ToString()));

                string outputFileName = FileOps.getInputFileName(fileName);
                outputFileName = outputFileName + "-graded.txt";

                // Even if errors in some lines, still print correctly parsed lines
                FileOps.writeLinesToFile(outputFileName, outputLines.ToArray());

                //Finish
                if (students.Count == lines.Length)
                {
                    Console.WriteLine("Finished: Created " + outputFileName);
                }
                else
                {
                    Console.WriteLine("Finished: Created " + outputFileName +" with errors");
                }
                
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine("File not found, Please give full path of an existing file");
            }

        }

        /// <summary>
        /// Parses an input line form text file to generate a Student instance. 
        /// Validates the input line for correct input.
        /// </summary>
        /// <param name="textLine"></param>
        /// <param name="lineNum"></param>
        /// <returns></returns>
        public Student parseStudent(string textLine, int lineNum)
        {
            string[] values = textLine.Split(',');
            string errorMessage = "";
            Student student = null;
            if (values.Length != 3)
            {
                errorMessage = "Input File contains error in line:" + lineNum +
                    "\n Each line should be <FirstName>, <LastName>, <Score>";

            }
            else
            {
                int score;
                if (int.TryParse(values[2], out score))
                {
                    student = new Student();
                    student.FirstName = values[1].Trim();
                    student.LastName = values[0].Trim();
                    student.Score = score;
                }
                else
                {
                    errorMessage = "Input File contains error in line:" + lineNum +
                        "\n Third value should be an Integer";
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                Console.Error.WriteLine(errorMessage);
                return null;
            }
            else
            {
                return student;
            }
        }

        /// <summary>
        /// Given the array of input lines from file, create a list of Student from it
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public List<Student> parseStudents(string[] lines)
        {
            List<Student> students = new List<Student>();
            for (int i = 0; i < lines.Length; i++)
            {
                Student student = parseStudent(lines[i], i + 1);
                if (student != null)
                {
                    students.Add(student);
                }
                else
                {
                    continue;
                }
            }

            return students;
        } 
    }
}
